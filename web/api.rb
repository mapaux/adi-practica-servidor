# encoding: utf-8
require 'sinatra/base'
require 'sinatra/mustache'
require_relative '../datos/init_datamapper'
require_relative '../negocio/peticion_service'
require_relative '../negocio/usuario_service'
require_relative '../negocio/firma_service'
require_relative '../negocio/actualizacion_service'

class ServidorAPI < Sinatra::Base
  #Método que permite recibir los datos de un usuario concreto en formato JSON
  get '/usuarios/:login' do
  	puts 'Desde API, devolviendo datos usuario'
  	puts "#{params[:login]}"
    #Si tenemos el usuario registrado, devolveremos sus datos, sino saltará un 404
    user ||= Usuario.get(params[:login]) || halt(404)
    puts "#{user.nombre}"
    return user.to_json
  end

  #Método que indica si un login está ya en uso o no
  get '/loginDisponible/:login' do
  	puts 'Desde API, devolviendo si un nombre de usuario está disponible'
    #Almacenamos en una variable los datos del usuario que le hemos pasado
    @usuario = UsuarioService.new.mostrar_usuario(params[:login])
    #Si la variable no contiene datos implica que no se encuentra ese usuario en nuestra base de datos
    if @usuario.empty?
      puts "usuario disponible"
      return 'OK'
    else
      puts "usuario NO disponible"
      return 'no'
    end
  end

  #Método que permite registrar un nuevo usuario en nuestra BBDD
  post '/usuarios' do
    puts 'Desde API, registrando un nuevo usuario'
    #Leemos los datos de la petición
    objeto = JSON.parse request.body.read
    
    #Si los campos están completos, creamos el usuario
    if !objeto['login'].empty? and !objeto['email'].empty? and !objeto['password'].empty? and !objeto['apellidos'].empty? and !objeto['nombre'].empty?
      usuario = UsuarioService.new.crear_usuario(objeto)
      puts "Usuario nuevo creado, hay que saludar a #{objeto['nombre']}, cuyo email es #{objeto['email']}"
      return usuario
    else
      #En caso contrario se manda un 400
      puts "Creación de usuario denegada, faltaban datos"
      halt 400
    end
  end

  #Método que permite la creación de una petición por parte de un usuario
  post '/peticiones' do
  	puts 'Desde API, creando una nueva petición'
  	#Leemos los datos de la petición
    peticion = JSON.parse request.body.read
    
    puts "petición leida"

    #Si la petición la lanza algún usuario sin loguear, le devolvemos un 403
    if session['usuario'] == ''
      puts "usuario NO logueado"
      halt 403
    else
      puts "usuario logueado"

      #En caso contrario, se comprueba que estén todos los datos y se procede a crear la petición
      if !peticion['titulo'].empty? and !peticion['fin'].empty? and !peticion['texto'].empty? and !peticion['firmasObjetivo'].empty?        
        #Petición creada
        peticion_nueva = PeticionService.new.crear_peticion(peticion)
        #Y se asigna la petición al usuario correspondiente, su creador
        UsuarioService.new.agregar_peticion(session['usuario'], peticion_nueva)

        #Si todo ha ido bien se devuelve un 201
        if(peticion_nueva)
          puts "petición creada"
          status 201          
        end 
      #Si falta algún dato, se muestra un 400       
      else
        puts "algun campo vacio"
        halt 400
      end
    end
  end

  #Método que permite la firma de peticiones
  post '/peticiones/:id/firmas/' do
  	puts 'Desde API, firmando una petición'
  	#Leemos los datos de la petición
    firma = JSON.parse request.body.read

    puts "firma leida"
    
    #Si el usuario que quiere firmar está logueado, se cogen sus datos desde la sesión 
    #y se refleja que la firma está autenticada
    if session['usuario'] != ''
      puts "firma un usuario logueado"
      usuario = Usuario.first(:login => session['usuario'])  
      firma['nombre'] = usuario.nombre
      firma['apellidos'] = usuario.apellidos
      firma['email'] = usuario.email
      firma['tipo'] = 'Autenticada'
    #En caso de no estar logueado, esos datos estarán ya en la firma y se refleja que no está registrado
    else
      puts "firma un usuario NO logueado"
      firma['tipo'] = 'No autenticada'
    end 

    #Se crea la firma
    firma_final = FirmaService.new.crear_firma(firma)

    #Si la firma pertenece a un usuario registrado se le asigna esta relación
    if session['usuario'] != ''
      UsuarioService.new.agregar_firma(usuario, firma_final)
    end

    #También se asigna la firma con la petición correspondiente
    PeticionService.new.agregar_firma(params[:id], firma_final)
    
    #Si todo ha ido bien se devuelve un 201, en caso contrario un 400
    if firma_final
      status 201
    else
      status 400
    end
  end

  #Método que devuelve el listado de actualizaciones correspondientes a una petición
  get '/peticiones/:id/actualizaciones' do
    #Obtiene las actualizaciones, devuelve un 200 y las manda en formato json
    actualizaciones = ActualizacionService.new.mostrar_actualizaciones(params[:id])
    status 200
    actualizaciones.to_json
  end

  #Método que permite añadir nuevas actualizaciones a una petición
  post '/peticiones/:id/actualizaciones' do
    #Leemos los datos de la petición
    actualizacion = JSON.parse request.body.read

    puts "actualización leida"
    
    #Se crea la nueva actualización
    actualizacion_final = ActualizacionService.new.crear_actualizacion(actualizacion)
    #La nueva actualización se asigna a la petición correspondiente
    PeticionService.new.agregar_actualizacion(params[:id], actualizacion_final)

    status 201
  end

  #Método que permite actualizar una actualización
  put '/peticiones/:id/actualizaciones/:id_actualizacion' do
    #Leemos los datos de la petición
    nueva_actualizacion = JSON.parse request.body.read

    #Se llama al método de actualización con los nuevos datos
    ActualizacionService.new.actualizar_actualizacion(params[:id_actualizacion], nueva_actualizacion)

    status 201
  end

  #Método que permite borrar una actualización de una petición
  #Generalmente no borra a la primera, sino a la segunda, no es un bug, es una feature, aporta más
  #"seguridad" por si le has dado sin querer
  delete '/peticiones/:id/actualizaciones/:id_actualizacion' do
    #Se llama al método de borrar, y si todo ha ido bien, devuelve un 201, sino un 400
    if ActualizacionService.new.borrar_actualizacion(params[:id_actualizacion])
      status 201
    else
      status 400
    end
  end

end

