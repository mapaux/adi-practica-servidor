/*
Función que nos muestra el lightbox en el cual hacemos el login y el fadebox trasero que cubre el resto de la interfaz
 */
function showLightbox() {
    document.getElementById('over').style.display='block';
    document.getElementById('fade').style.display='block';
}

/*
 Función que nos muestra el lightbox en el cual hacemos el registro y el fadebox trasero que cubre el resto de la interfaz
 */
function showLightbox2() {
    document.getElementById('over2').style.display='block';
    document.getElementById('fade').style.display='block';
}

/*
 Función que nos muestra el lightbox en el cual creamos la nueva petición (para abrirlo hay que estar logueado)
 y el fadebox trasero que cubre el resto de la interfaz
 */
function showLightBoxPeticion() {
    if(localStorage.userLog != ""){
        document.getElementById('overpeticion').style.display='block';
        document.getElementById('fade').style.display='block';
    }
}

/*
 Función que nos oculta todos los lightbox de los cuales disponemos en la aplicación web y el fadebox trasero, así con un método
 podemos reutilizarlo para todas las ocasiones que necesitemos.
 Además, se ocultan algunos errores que no tienen por qué verse una vez cerrado el formulario
 */
function hideLightbox() {
    document.getElementById('over').style.display='none';
    document.getElementById('over2').style.display='none';
    document.getElementById('overpeticion').style.display='none';
    document.getElementById('fade').style.display='none';
    document.getElementById('aviso_reg').innerHTML = "";
    document.getElementById('aviso_log').innerHTML = "";
    document.getElementById('aviso_pet').innerHTML = "";
}

/*
 Función que nos muestra en la barra superior el aspecto que indica que un usuario está conectado, aparte de ocultar la otra posible vista
 */
function mostrarBarraLog() {
    document.getElementById('nombreUserLog').innerHTML = localStorage.userLog;
    document.getElementById('noLog').style.display='none';
    document.getElementById('userLog').style.display='block';
}

/*
 Función que nos muestra en la barra superior el aspecto que indica que un usuario NO está conectado, aparte de ocultar la otra posible vista
 */
function mostrarBarraNoLog() {
    document.getElementById('noLog').style.display='block';
    document.getElementById('userLog').style.display='none';
}

/*
 Función que nos muestra un error en el registro de un usuario
 */
function mostrarErrorReg() {
    document.getElementById('aviso_reg').innerHTML = "Datos erróneos o incompletos";
}

/*
 Función que nos muestra un error en el registro de un usuario causado porque las contraseñas no coinciden
 */
function mostrarErrorPassReg() {
    document.getElementById('fallo_pass_reg').innerHTML = "La contraseña debe ser la misma";
    document.getElementById('fallo_pass_reg2').innerHTML = "La contraseña debe ser la misma";
}

/*
 Función que oculta el error de que las contraseñas son diferentes en el registro del usuario
 */
function quitarErrorPassReg() {
    document.getElementById('fallo_pass_reg').innerHTML = "";
    document.getElementById('fallo_pass_reg2').innerHTML = "";
}

/*
 Función que nos muestra un error en el registro de un usuario si ese nombre ya está reservado
 */
function mostrarErrorUserYaRegistrado(){
    document.getElementById("fallo_mail_reg").innerHTML = "Correo ya registrado";
}

/*
 Función que nos oculta el error de que el usuario ya está en uso
 */
function ocultarErrorUserYaRegistrado(){
    document.getElementById("fallo_mail_reg").innerHTML = "";
}

function mostrarFormularioLogeado(){
    document.getElementById("namePeti").style.display='none';
    document.getElementById("emailPeti").style.display='none';

}

function mostrarFormularioNoLogeado(){
    document.getElementById("namePeti").style.display='block';
    document.getElementById("emailPeti").style.display='block';
}

/*
 Función que nos muestra un error en la creación de una firma
 */
function mostrarErrorSig(){
    document.getElementById("aviso_Sig").style.color = "red";
    document.getElementById("aviso_Sig").innerHTML = "Debes completar todos los campos correctamente";
}

/*
 Función que nos muestra que una firma ha sido concretamente realizada, un texto se nos desplaza por la pantalla informándonos para desaparecer
 al poco rato, y nos resetea los campos de la firma para no crear otra idéntica al pulsar dos veces muy rápido (filtro anti-ansias)
 */
function renovarTrasFirma(){
    document.getElementById("aviso_Sig").style.color = "green";
    animarTexto("aviso_Sig", "Petición creada con éxito");
    document.getElementById("nombreSig").value = "";
    document.getElementById("apellidosSig").value = "";
    document.getElementById("emailSig").value = "";
    document.getElementById("textoMotivo").value = "";
    document.getElementById("verifySig").value = "";
    document.getElementById("textoMotivoLogueado").value = "";
    document.getElementById("verifySigLog").value = "";
}

/*
 Función que nada más cargar la página de una petición comprueba qué barra de navegación superior debe mostrar y muestra un formulario para firmar
 la petición acorde a si estamos logueados o no (si estamos logueados aparecen menos campos)
 */
function verLoginPet(){
    if(localStorage.userLog == ""){
        mostrarBarraNoLog();
        //mostrarFormularioNoLogeado()
    }
    else{
        mostrarBarraLog();
        //mostrarFormularioLogeado()
    }
    var m = new Firma();
    var v = new FirmaView({model: m});
    v.render();
    document.getElementById("aviso_Sig").innerHTML = "";
}

/*
 Función que nada más cargar la página inicial nos muestra la barra superior dependiendo de si estamos logueados o no
 */
function verLog(){
    if(localStorage.userLog == ""){
        mostrarBarraNoLog();
    }
    else{
        mostrarBarraLog();
    }
}

/*
 Función que lleva a cabo el login del usuario
 */
function doLogIn(){
    //Obtenemos los datos que nos suministra el usuario
    var user = document.getElementById("mail");
    var pass = document.getElementById("pass");

    //Si uno de los dos elementos está vacio, mostramos un error
    if(user.value.length == 0 || pass.value.length == 0){
        document.getElementById('aviso_log').innerHTML = "Faltan datos";
    }
    else{
        //En caso contrario abrimos una petición de método POST
        var req = new XMLHttpRequest();
        req.open('POST', "auth/login", true);
        req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        //Lo mandamos a la API correspondiente
        req.send('login='+user.value+'&password='+pass.value);

        req.onreadystatechange = function(){
            if(req.readyState == 4){
                if(req.status == 200){
                    /*Si el estado de la petición indica que ha ido bien, abimos una nueva petición en la cual recogeremos el nombre y apellidos
                    del usuario para almacenarlo en un localStorage*/
                    var req2 = new XMLHttpRequest();
                    req2.open('GET', 'api/usuarios/'+user.value, true);
                    req2.onreadystatechange = function(){
                        if(req2.readyState == 4){
                            if(req2.status == 200){
                                //Una vez que hemos recogido los datos, los parseamos, los almacenamos y lanzamos el método correspondiente a cargar
                                // de nuevo la página para que modifique los elementos de la interfaz
                                var texto = JSON.parse(req2.responseText);
                                var persona = texto.nombre + " " + texto.apellidos;
                                localStorage.userLog = persona;
                                localStorage.email = user.value;
                                document.getElementById('nombreUserLog').innerHTML = localStorage.userLog;
                                //onload();
                                location.reload();
                            }
                        }
                    }
                    req2.send(null);
                    //Animamos el texto del Login, para que nos muestre que ha ido bien en color verde y al pasar un tiempo desaparezca
                    animarTextoLogin();

                }
                //Si los datos no son correctos mostramos un fallo
                else if(req.status == 403){
                    document.getElementById('aviso_log').innerHTML = "Usuario o contraseña incorrectos";
                }
            }
        }
    }

}

/*
 Función que cerramos la sesión del usuario
 */
function doLogOut(){
    //Vaciamos el localStorage
    localStorage.userLog = "";
    localStorage.email = "";

    //Lanzamos una petición GET para llevar a cabo el logout y cargamos los elementos de la interfaz que correspondan
    var req = new XMLHttpRequest();
    req.open('GET', 'auth/logout', true);
    req.onreadystatechange = function(){
        if(req.readyState == 4){
            if(req.status == 200){
                verLog();
                location.reload();
            }
        }
    }
    req.send(null);

    onload();
}

/*
 Función que nos permite registrar un nuevo usuario
 */
function doRegistry(){

    //Antes que nada, miramos el correo que ha marcado el usuario
    var correo = document.getElementById("mailReg").value;
    var req_previa = new XMLHttpRequest()
    req_previa.open('GET', 'api/loginDisponible/'+correo, true);
    req_previa.onreadystatechange = function(){
        if(req_previa.readyState == 4){
            if(req_previa.status == 200){
                var respuesta = req_previa.responseText;
                //Si recibimos un "OK" como respuesta quiere decir que ese correo está disponible
                if(respuesta == "OK"){
                    //Recogemos todos los datos del usuario
                    var nomUser = document.getElementById("nombreReg").value;
                    var apellidoUser = document.getElementById("apellidosReg").value;
                    var mailUser = document.getElementById("mailReg").value;
                    var passUser = document.getElementById("passReg").value;
                    var passUser2 = document.getElementById("passReg2").value;

                    //Si las dos contraseñas son iguales avanzamos
                    if(passUser == passUser2){
                        var req = new XMLHttpRequest();
                        req.open('POST', 'api/usuarios', true);
                        req.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
                        //Creamos un nuevo objeto con los datos del registro
                        var registro = new Object();
                        registro.login = mailUser;
                        registro.email = mailUser;
                        registro.password = passUser;
                        registro.nombre = nomUser;
                        registro.apellidos = apellidoUser;

                        req.onreadystatechange = function(){
                            if(req.readyState == 4){
                                if(req.status == 201){
                                    //Si el registro es correcto, limpiamos los posibles fallos y cerramos la ventana
                                    document.getElementById("aviso_reg").innerHTML = "";
                                    hideLightbox();
                                }
                                if(req.status == 400){
                                    //Si algo es incorrecto, mostramos un aviso de error
                                    document.getElementById("aviso_reg").innerHTML = "Fallo en el registro, algún dato no es correcto";
                                }
                            }
                        }
                        //Mandamos el objeto en formato JSON
                        req.send(JSON.stringify(registro));


                        //Una vez esté correcto, ocultamos la ventana de registro y nos aseguramos que los errores estan vacios
                        quitarErrorPassReg();
                        hideLightbox();
                    }
                    //Si las contraseñas son diferentes, lo avisamos y esperamos
                    else{
                        mostrarErrorPassReg();
                    }
                }
                //En caso de que el nombre sea incorrecto y queramos registrarnos, mostrará un fallo
                else{
                    document.getElementById("aviso_reg").innerHTML = "Fallo en el registro, algún dato no es correcto";
                }
            }
            else
            mostrarErrorReg()
        }
    }
    req_previa.send(null);
}

/*
 Función que nos indica si un correo está ya en unso o no para el registro. En caso de no estar disponible, muestra aviso de error
 */
function comprobarLogin(){
    //Lanzamos una petición a la api para comprobar si el correo está disponible
    var correo = document.getElementById("mailReg").value;
    var req = new XMLHttpRequest()
    req.open('GET', 'api/loginDisponible/'+correo, true);
    req.onreadystatechange = function(){
        if(req.readyState == 4){
            if(req.status == 200){
                var respuesta = req.responseText;
                //MALDITAS CLAUSURAS, SON EL MAL

                //En el caso de que el nombre no esté ya en uso, recibimos un "OK" y ocultamos el error
                if(respuesta == "OK"){
                    ocultarErrorUserYaRegistrado();
                }
                //En caso contrario, mostramos el error indicado
                else{
                    mostrarErrorUserYaRegistrado();
                }
            }
        }
    }
    req.send(null);
}

/*
 Función que nos permite realizar una nueva petición
 */
function doPeticion() {
    var req = new XMLHttpRequest()
    req.open('POST', 'api/peticiones', true);
    req.setRequestHeader('Content-Type', 'application/json; charset=utf-8');

    //Almacenamos los datos insertados en un nuevo objeto
    var peticion = new Object();
    peticion.titulo = document.getElementById('titlePet').value;
    peticion.fin = document.getElementById('datePet').value;
    peticion.texto = document.getElementById('textPet').value;
    peticion.firmasObjetivo = document.getElementById('signaturesObjPet').value;


    req.onreadystatechange = function(){
        if(req.readyState == 4){
            //Si ha ido bien, ocultamos el lightbox
            if(req.status == 201){
                hideLightbox();
            }
            if(req.status == 403){
                hideLightbox();
            }
            //Si algún campo falla, mostramos un aviso
            if(req.status == 400){
                document.getElementById('aviso_pet').innerHTML = "Algún elemento es incorrecto";
            }
            //Si falla nuestro servidor también mostramos el aviso
            if(req.status == 500){
                document.getElementById('aviso_pet').innerHTML = "Error interno del servidor, inténtalo de nuevo más adelante";
            }
        }
    }

    req.send(JSON.stringify(peticion))

}

/*
 Función que nos permite firmar una petición
 */
function doSignature(id_peticion){

    var firma = new Object();
    var correcto = false;

    //Actuamos de manera diferente dependiendo de si el usuario está logueado o no
    //Tenemos un objeto que vamos a mandar, el cual tendrá más o menos datos dependiendo los solicitados
    //Si el usuario no está logueado, pedimos nombre, apellidos y correo, aparte del texto y la petición de firma pública
    if(localStorage.userLog == ""){
        firma.nombre = document.getElementById('nombreSig').value;
        firma.apellidos = document.getElementById('apellidosSig').value;
        firma.email = document.getElementById('emailSig').value;
        firma.publica = document.getElementById('verifySigLog').checked;
        firma.comentario = document.getElementById('textoMotivo').value;
        if(firma.nombre != "" && firma.apellidos != "" && firma.email != "" && firma.comentario != ""){
            correcto = true;
        }
    }
    //En cambio si ya estamos logueados, pedimos solo el texto y la petición de firma pública, puesto que el resto lo manda automáticamente la sesión
    else{
        firma.publica = document.getElementById('verifySig').checked;
        firma.comentario = document.getElementById('textoMotivo').value;
        if(firma.comentario != ""){
            correcto = true;
        }
    }

    //Si los datos han sido catalogados anteriormente como correctos, lanzamos la petición
    if(correcto){
        var req = new XMLHttpRequest()
        req.open('POST', 'api/peticiones/'+id_peticion+'/firmas/', true);
        req.setRequestHeader('Content-Type', 'application/json; charset=utf-8');

        req.onreadystatechange = function(){
            if(req.readyState == 4){
                //Si la firma ha sido un éxito, mostramos el aviso animado y limpiamos el formulario
                if(req.status == 201){
                    onload();
                    //renovarTrasFirma();
                }
                //Si ha habido algún error, lo indicamos en la pantalla
                if(req.status == 400){
                    mostrarErrorSig();
                }
                //Si el error es del servidor, también lo indicamos
                if(req.status == 500){
                    document.getElementById("aviso_Sig").style.color = "red";
                    document.getElementById("aviso_Sig").innerHTML = "Ha ocurrido un error interno en el servidor, vuelve a intentarlo en unos minutos";
                }

            }
        }

        req.send(JSON.stringify(firma))
    }
    //Si los datos no habían sido catalogados como correctos, lo mostramos por pantalla
    else{
        mostrarErrorSig();
    }
}

/*
 Función que permite animar el texto que indica que la petición ha recibido la firma con éxito
 */
function animarTexto(id, texto){
    var elemento = document.getElementById(id);
    var pos = 0;
    //Situamos el elemento en su posición original
    elemento.style.left = pos+"px";
    //Hacemos que el contenido sea el mismo que el parámetro que hemos recibido
    elemento.innerHTML = texto;
    //Establecemos un intervalo, esto se va a realizar cada 4ms (el límite inferior)
    var timer = setInterval(function(){
        //Si aún no hemos llegado hasta la posición que queríamos, aumentamos en 1 la posición para desplazar el texto
        if(pos < 200){
            elemento.style.left = pos+"px";
            pos++;
        }
        //Una vez hemos llegado al destino, limpiamos el intervalo y vaciamos el texto para que desaparezca
        else{
            clearInterval(timer);
            elemento.innerHTML = "";
        }
    }, 4);
}

/*
 Función que nos muestra un mensaje de que se ha realizado el login de manera correcta àra desaparecer al tiempo
 */
function animarTextoLogin(){
    var elemento = document.getElementById('aviso_log');
    //Cambiamos el color al texto y escribimos nuestro mensaje
    elemento.style.color = "green";
    elemento.innerHTML = "Login correcto";

    var pos = 0;
    //Establecemos el intervalo
    var timer = setInterval(function(){
        //Hasta que este contador no llegue al número marcado esperamos
        if(pos < 300){
            pos++;
        }
        //Una vez ha pasado el tiempo, limpiamos el intervalo, vaciamos el texto y le cambiamos el error, ya que es un contenedor
        // donde también se muestran errores, aparte de ocultar el lightbox y el fadebox. Esto transmite un efecto de desaparición en el tiempo
        else{
            clearInterval(timer);
            elemento.innerHTML = "";
            elemento.style.color = "red";
            hideLightbox();
        }
    }, 4);
}

//ZONA BACKBONE, DANGER IN PROCESS

/*
Modelo de la Firma
 */
var Firma = Backbone.Model.extend({
    url: 'api/peticiones/'+id_peticion+'/firmas/'
})

/*
Vista de la firma
 */
var FirmaView = Backbone.View.extend({
    el: '#formulario',

    //Imprimimos el formulario de firma
    render: function(){
        if(localStorage.userLog == ""){
            this.el.innerHTML += '<p id="namePeti"><label class="label" ><span>Nombre:</span> <input type="text" id="nombreSig" placeholder="Escribe tu nombre" required="required"></label> <label class="label" ><span>Apellidos:</span> <input type="text" id="apellidosSig" placeholder="Apellidos" required="required" ></label></p>'
            this.el.innerHTML += '<p id="emailPeti"><label class="label" ><span>Email:</span> <input type="email" id="emailSig" placeholder="tucorreo@servidor.es" required="required" ></label></p>'
        }
        this.el.innerHTML += '<p id="textPeti"><label class="label" ><span>Motivo:</span> <textarea id="textoMotivo" placeholder="Escribe aquí" cols="17" rows="5" maxlength="1000" required="required"></textarea></label></p>'
        this.el.innerHTML += '<label class="checkbox"><input id="i_publica" type="checkbox"> Mostrar públicamente mi firma</label>'
        this.el.innerHTML += '<input class="btn" type="button" id="botonFirma" value="Firmar"> <span id="aviso_Sig" style="position: relative"></span>'
    },
    //Realizamos la funcion de firmar
    firmar: function(){
        var signature = new Firma()
        var texto = document.getElementById("textoMotivo").value
        console.log("patata")
        var verify = document.getElementById("i_publica").checked
        if(localStorage.userLog == ""){
            var nombre = document.getElementById("nombreSig").value
            var apellidos = document.getElementById("apellidosSig").value
            var email = document.getElementById("emailSig").value
            signature.set({nombre: nombre, apellidos: apellidos, email: email, comentario: texto, publica: verify})
        }
        else{
            signature.set({comentario: texto, publica: verify})
        }
        signature.save()
        location.reload()
    },
    //Controlamos los eventos para que se lancen al hacer click sobre el boton que nosotros queremos
    events: {
        "click #botonFirma": "firmar"
    }
})


//PARTE ACTUALIZACIONES

//Modelo de las actualizaciones
var ActualizacionModel = Backbone.Model.extend({

})

//Coleccion de las actualizaciones
var Actualizaciones = Backbone.Collection.extend({
    model: ActualizacionModel,
    url:'api/peticiones/'+id_peticion+'/actualizaciones'
})

//Vista de la actualizacion
var ActualizacionView = Backbone.View.extend({
    className:'actualizacion',


    //Plantilla si el usuario logueado es el mismo que creo la peticion
    template:Mustache.compile('<div class="span4"> <span><strong>{{fecha}}</strong></span> <p> {{contenido}} </p> <input type="button" class="editarActualizacion" value="Editar"><input type="button" class="borrarActualizacion" value="Borrar"></div>'),
    //template:Mustache.compile('<em>{{contenido}}</em><b> </b><em>{{fecha}}</em><hr>'),
    //plantilla para los usuarios no logueados y logueados que no con los que  crearon la peticion
    template2:Mustache.compile('<div class="span4"> <span><strong>{{fecha}}</strong></span> <p> {{contenido}} </p></div>'),
    //plantilla para editar una actualizacion
    templateEditar:Mustache.compile('<div class="span4"> <span><input type="text" id="editaract" value="{{contenido}}"> </span><br> <input type="button" id="guardarCambios" value="Guardar"></div>'),


    //muestra las actualizaciones
    render: function() {
        //si el usuario esta logueado, mostramos el template con permisos

        if(document.getElementById("creador").value==localStorage.email){
            this.el.innerHTML=this.template(this.model.toJSON())
        }
        //si no es el usuario logueado, mostramos la vista normal
        else{
            this.el.innerHTML=this.template2(this.model.toJSON())
        }
    },

    //evento de borrar editar y guardar
    events: {
        "click .borrarActualizacion":'Borrar',
        "click .editarActualizacion":'Editar',
        "click #guardarCambios": 'Guardar'
    },

    //borrar una actualizacion
    Borrar:function(){
        this.model.destroy()
        this.remove()
        location.reload();
    },

    //Te muestra la plantilla de editar
    Editar:function(){
        //editamos esta vista para que nos muestre el editar
        this.el.innerHTML=this.templateEditar(this.model.toJSON())
    },

    //Guarda los cambios en una actualizacion
    Guardar:function(){
        //cambiamos el contenido de la actualizacion
        this.model.set("contenido",document.getElementById("editaract").value)
        //lo guardamos y lo mostramos
        this.model.save()
        this.render()
    }

})
//Vista global de las actualizaciones
var ActualizacionesView =Backbone.View.extend({
    initialize:function(){
        this.collection = new Actualizaciones()
        _.bindAll(this,"renderActu")
        this.collection.fetch({reset:true})
        this.listenTo(this.collection,"reset",this.render)
    },


    el:"#actua",

    //dibujamos la vista llamando a los elementos de la colecccion
    render:function(){
        this.collection.each(this.renderActu)
    },

    renderActu:function(modelo){
        //creamos a vista de la actualizacion
        var actualizacionView= new ActualizacionView({model:modelo})
        //la mostramos
        actualizacionView.render()
        this.el.appendChild(actualizacionView.el)
    },

    //funcion para añadir una nueva actualizacion
    altaActu:function(){
        //creamos el modelo de la actualizacion
        var actualizacion=new ActualizacionModel();
        //rellenamos los datos
        actualizacion.set("contenido",document.getElementById("contenidoActu").value)
        actualizacion.set("peticion",id_peticion)
        //la añadimos a la coleccion y la guardamos
        this.collection.add(actualizacion)
        actualizacion.save()
        //mostramos en pantalla
        this.renderActu(actualizacion)
        console.log(actualizacion)
        location.reload();
    },

    //evento añadir una actualizacion
    events:{
        "click #altaActu":"altaActu"
    }

})

var actView;
actView= new ActualizacionesView()
console.log("Allá vamos, que sea lo que el navegador quiera...")