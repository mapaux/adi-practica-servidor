# encoding: utf-8
require 'sinatra/base'
require 'sinatra/mustache'
require 'data_mapper'
require 'json'

require_relative '../datos/init_datamapper'
require_relative '../negocio/peticion_service'
require_relative '../negocio/usuario_service'

class ServidorPlantillas < Sinatra::Base
  #Si no hay nada en la URL de la aplicación salvo el directorio raíz, se redirige al índice
  get '/' do
    puts "Redirigimos desde '/' hasta la página principal"
    redirect '/muevete/index'
  end

  #Si se accede al index, se muestran las peticiones detacadas
  get '/index' do
    @destacadas = PeticionService.new.listar_destacadas
    mustache :index
  end

  #Si se accede a una petición, se recoge la id suministrada y se busca esa petición para mostrarla
  get '/peticion' do
    puts 'Sirviendo peticion concreta'
    @peticion = PeticionService.new.listar_peticion(params[:id])
    mustache :peticion
  end

  #Se muestra el usuario indicado en la URL
  get '/usuario' do
    puts 'Mostrando página del usuario'    
    @usuario = UsuarioService.new.mostrar_usuario(params[:login])
    mustache :usuario
  end

  configure do
    puts "Arrancando la aplicación..."
    puts "Iniciado DataMapper..."
    init_datamapper
    puts "Tipo archivo con extensión .html establecido..."
    Tilt.register Tilt::MustacheTemplate, 'html'
  end

end










