# encoding: utf-8
require 'sinatra/base'
require 'sinatra/mustache'
require 'data_mapper'
require 'json'

require_relative '../datos/init_datamapper'
require_relative '../negocio/peticion_service'
require_relative '../negocio/usuario_service'

class ServidorAutentificacion < Sinatra::Base
  
  #Método que permite que los usuarios se logueen
  post '/login' do
  	puts 'Desde auth, logueando a un usuario'

    #Si los campos no están vacios, procedemos a intentar loguear
    if !params[:login].empty? and !params[:password].empty?      
      #Buscamos si hay algún usuario con esa combinación de login y password
      @usuario = UsuarioService.new.mostrar_usuario_login(params[:login], params[:password])
      #Si no existe, devolvemos un 403
      if @usuario.empty?
        puts "Datos incorrectos"
        halt 403
      #Si existe, creamos una nueva sesión con el correo del usuario
      else
        puts "Sesión creada"
        session['usuario'] = params[:login]
        status 200
      end
    else
      puts "Faltan datos"
      halt 400
    end 	  
  end

  #Método que permite cerrar la sesión de trabajo de un usuario
  get '/logout' do
  	puts 'Desde auth, cerrando sesion de un usuario'
    #Borramos todos los datos de la sesión
  	session.clear
  	
    status 200    
  end

  configure do
    use Rack::Session::Pool
  end

end