require 'data_mapper'

class Peticion
  include DataMapper::Resource

  property :id, Serial
  property :abierta, Boolean, :default => 'true'
  property :conseguida, Boolean, :default => 'false'
  property :destacada, Boolean, :default => 'false'
  property :fin, Date
  property :firmasConseguidas, Integer, :default => '0'
  property :firmasObjetivo, Integer
  property :inicio, Date
  property :texto, String
  property :titulo, String

  belongs_to :usuario

  has n, :firmas, 'Firma'
  has n, :actualizaciones, 'Actualizacion'

end