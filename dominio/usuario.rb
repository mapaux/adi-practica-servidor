require 'data_mapper'

class Usuario
  include DataMapper::Resource

  property :login, String, :key => true
  property :email, String
  property :password, String, :required => true
  property :apellidos, String
  property :nombre, String
  property :rol, String

  has n, :peticiones, 'Peticion'
  has n, :firmas, 'Firma'

end