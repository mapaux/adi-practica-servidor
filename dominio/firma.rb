require 'data_mapper'

class Firma
  include DataMapper::Resource

  property :id, Serial
  property :comentario, String
  property :fecha, Date  
  property :tipo, String
  property :publica, Boolean
  property :apellidos, String
  property :email, String
  property :nombre, String


  belongs_to :usuario, :required => false
  belongs_to :peticion

end