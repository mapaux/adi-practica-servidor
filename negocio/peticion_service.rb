require_relative '../datos/PeticionDAO'

class PeticionService
  
  #Método que devuelve únicamente las peticiones destacadas
  def listar_destacadas()
    PeticionDAO.new.listar_destacadas
  end

  #Método que devuelve una petición concreta
  def listar_peticion(id)
  	PeticionDAO.new.listar_peticion(id)
  end

  #Método que crea una nueva petición
  def crear_peticion(peticion)
  	PeticionDAO.new.crear_peticion(peticion)
  end

  #Método que asocia una firma con una petición
  def agregar_firma(peticion, firma_final)
    PeticionDAO.new.agregar_firma(peticion, firma_final)
  end

  #Método que asocia una actualización con una petición
  def agregar_actualizacion(id, actualizacion_final)
    PeticionDAO.new.agregar_actualizacion(id, actualizacion_final)
  end


end