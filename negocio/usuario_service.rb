require_relative '../datos/UsuarioDAO'

class UsuarioService

  #Método que permite crear nuevos usuarios
  def crear_usuario(objeto)
    UsuarioDAO.new.crear_usuario(objeto)
  end

  #Método que devuelve información de un usuario concreto
  def mostrar_usuario(login)
    UsuarioDAO.new.mostrar_usuario(login)
  end

  #Método que devuelve un usuario si el nombre y la contraseña son correctos
  def mostrar_usuario_login(login, password)
    UsuarioDAO.new.mostrar_usuario_login(login, password)
  end

  #Método que asocia una petición con el usuario
  def agregar_peticion(usuario, peticion)
    UsuarioDAO.new.agregar_peticion(usuario, peticion)
  end

  #Método que asocia una firma con un usuario
  def agregar_firma(usuario, firma_final)
  	UsuarioDAO.new.agregar_firma(usuario, firma_final)
  end
  
end