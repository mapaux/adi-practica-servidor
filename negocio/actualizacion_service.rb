require_relative '../datos/ActualizacionDAO'

class ActualizacionService
  #Método que muestra las actualizaciones de la petición mandada (id)
  def mostrar_actualizaciones(id)
    ActualizacionDAO.new.mostrar_actualizaciones(id)
  end

  #Método que crea una nueva actualización
  def crear_actualizacion(actualizacion)
    ActualizacionDAO.new.crear_actualizacion(actualizacion)
  end

  #Método que permite actualizar una petición ya existente
  def actualizar_actualizacion(id, actualizacion)
    ActualizacionDAO.new.actualizar_actualizacion(id, actualizacion)
  end

  #Método que permite borrar una actualización
  def borrar_actualizacion(id)
    ActualizacionDAO.new.borrar_actualizacion(id)
  end


end