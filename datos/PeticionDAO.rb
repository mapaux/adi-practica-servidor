require_relative '../dominio/peticion'
require 'date'

class PeticionDAO
  
  #Método que devuelve únicamente las peticiones destacadas
  def listar_destacadas()
    #Devuelve todas las peticiones con el booleano activado
    Peticion.all(:destacada=>true)
  end

  #Método que devuelve una petición concreta
  def listar_peticion(id)
  	puts "#{id}"
    Peticion.all(:id=>id)
  end

  #Método que crea una nueva petición
  def crear_peticion(peticion)
    Peticion.create(
    	:titulo =>             peticion['titulo'],
      :fin =>                peticion['fin'],
      :texto =>              peticion['texto'],
      :firmasObjetivo =>     peticion['firmasObjetivo'],
      :inicio =>             DateTime.now
    )
  end

  #Método que asocia una firma con una petición
  def agregar_firma(id, firma_final)
    peticion = Peticion.first(:id => id)
    #Añade la firma al final del listado de firmas de la petición
    peticion.firmas<<firma_final
    #Añade 1 al contador de firmas conseguidas
    peticion.firmasConseguidas += 1
    #Almacena estos cambios
    peticion.save
  end

  #Método que asocia una actualización con una petición
  def agregar_actualizacion(id, actualizacion_final)
    peticion = Peticion.first(:id => id)
    #Se agrega la actualización al final del listado que hay en la petición
    peticion.actualizaciones<<actualizacion_final
    #Se almacenan los cambios
    peticion.save
  end

end