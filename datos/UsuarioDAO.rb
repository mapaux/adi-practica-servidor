require_relative '../dominio/usuario'

class UsuarioDAO

  #Método que permite crear nuevos usuarios
  def crear_usuario(objeto)
    Usuario.create(
        :login =>         objeto['login'],
        :email =>         objeto['email'],
        :password =>      objeto['password'],
        :apellidos =>     objeto['apellidos'],
        :nombre =>        objeto['nombre'],
        :rol =>           'registrado'
      )
  end

  #Método que devuelve información de un usuario concreto
  def mostrar_usuario(login)
  	puts "#{login}"
    Usuario.all(:login=>login)
  end

  #Método que devuelve un usuario si el nombre y la contraseña son correctos
  def mostrar_usuario_login(login, password)
  	puts "#{login}"
    Usuario.all(:login=>login, :password=>password)
  end

  #Método que asocia una petición con el usuario
  def agregar_peticion(usuario_login, peticion)  
    #Se obtiene el usuario concreto
    usuario = Usuario.first(:login => usuario_login)	
    #se le añade la petición
    usuario.peticiones<<peticion
    #Se guardan los cambios
    usuario.save
  end

  #Método que asocia una firma con un usuario
  def agregar_firma(usuario, firma_final)
    #se añade la firma al listado del usuario
    usuario.firmas<<firma_final
    #Se almacenan los cambios
    usuario.save
  end

end