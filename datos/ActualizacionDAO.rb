require_relative '../dominio/actualizacion'
require 'date'

class ActualizacionDAO

  #Método que muestra las actualizaciones de la petición mandada (id)
  def mostrar_actualizaciones(id)
    #Devuelve todas las actualizaciones cuya petición coincida con la suministrada
    Actualizacion.all(:peticion_id => id)
  end

  #Método que crea una nueva actualización
  def crear_actualizacion(actualizacion)
    Actualizacion.create(
      :contenido => actualizacion['contenido'],
      :fecha => DateTime.now
      )
  end

  #Método que permite actualizar una petición ya existente
  def actualizar_actualizacion(id, actualizacion)
    #Obtiene la actualización concreta y lanza el update
    actualizacion_objetivo = Actualizacion.get(id)
    actualizacion_objetivo.update(
      :contenido => actualizacion['contenido']
      )
    actualizacion_objetivo.save
  end

  #Método que permite borrar una actualización
  def borrar_actualizacion(id)
    #Obtiene la actualización concreta y lanza el destroy
    actualizacion_objetivo = Actualizacion.get(id)
    actualizacion_objetivo.destroy()
  end

end