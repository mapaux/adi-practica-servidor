require_relative '../dominio/firma'
require 'date'

class FirmaDAO
  #Método que permite crear una nueva firma
  def crear_firma(firma)
    Firma.create(
      :comentario => firma['comentario'],
      :fecha => DateTime.now,
      :tipo => firma['tipo'],
      :publica => firma['publica'],
      :apellidos => firma['apellidos'],
      :email => firma['email'],
      :nombre => firma['nombre']
      )
  end

end